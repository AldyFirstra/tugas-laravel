@extends('layout.master')
@section('judul')
    Edit Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="nama">Nama:</label>
      <input type="text" class="form-control" id="nama" name="nama" placeholder="" value="{{$cast->nama}}">
      @error('nama')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur:</label>
        <input type="text" class="form-control" id="umur" name="umur" placeholder="" value="{{$cast->umur}}">
        @error('umur')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio:</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
        @error('bio')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection