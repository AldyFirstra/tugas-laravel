@extends('layout.master')
@section('judul')
    Tambah Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama:</label>
      <input type="text" class="form-control" id="nama" name="nama" placeholder="">
      @error('nama')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur:</label>
        <input type="text" class="form-control" id="umur" name="umur" placeholder="">
        @error('umur')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio:</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea>
        @error('bio')
          <div class="alert alert-danger">
              {{$message}}
          </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection